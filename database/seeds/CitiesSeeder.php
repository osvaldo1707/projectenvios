<?php

use Illuminate\Database\Seeder;
use App\City;
class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        City::truncate();
        $path = base_path().'/database/seeds/sql/cities.sql';
        $sql = file_get_contents($path);
        DB::unprepared($sql);
    }
}
