<?php

use Illuminate\Database\Seeder;
use App\CategoryProducts;

class CategoryProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CategoryProducts::truncate();
        $path = base_path().'/database/seeds/sql/category_products.sql';
        $sql = file_get_contents($path);
        DB::unprepared($sql);
    }
}
