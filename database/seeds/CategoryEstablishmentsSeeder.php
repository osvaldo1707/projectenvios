<?php

use Illuminate\Database\Seeder;
use App\CategoryEstablishment;
class CategoryEstablishmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CategoryEstablishment::truncate();
        $path = base_path().'/database/seeds/sql/category_estalishments.sql';
        $sql = file_get_contents($path);
        DB::unprepared($sql);
    }
}
