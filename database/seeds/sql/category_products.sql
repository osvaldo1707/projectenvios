/*
-- Query: SELECT * FROM jaiimesend.category_products
LIMIT 0, 1000

-- Date: 2019-09-29 16:51
*/
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (1,'Lacteos y Huevos ',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (2,'Frutas y Verduras',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (3,'Aguas y Té Liquidos',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (4,'Jugos y Néctares',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (5,'Refrescos y Sodas',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (6,'Beidas Energeticas',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (7,'Cafe',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (8,'Tortillas',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (9,'Panes y Pasteles',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (10,'Cerveza, Vinos y Licores',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (11,'Botanas',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (12,'Carnes Frias',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (13,'Carnes, Aves y Mariscos',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (14,'Bebes',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (15,'Cuidado Personal',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (16,'Mascotas',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (17,'Farmacia',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (18,'Hogar y Manualidades',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (19,'Jardin, Autos y Ferreterias',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (20,'Jugueterias',NULL,NULL);
INSERT INTO `category_products` (`id`,`name`,`created_at`,`updated_at`) VALUES (21,'Aseo del Hogar',NULL,NULL);
