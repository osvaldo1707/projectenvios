/*
-- Query: SELECT * FROM jaiimesend.establishments
LIMIT 0, 1000

-- Date: 2019-09-29 17:16
*/
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (1,'Taller De Motos Los Amigos De Zamora',10,NULL,NULL);
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (2,'Materiales \"El Trebol\"',11,NULL,NULL);
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (3,'Cuadros Gigantes \"Ross\"',13,NULL,NULL);
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (4,'Dulcelandia',13,NULL,NULL);
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (5,'Abarrotes Felipe',1,NULL,NULL);
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (6,'Bar Bohemio',2,NULL,NULL);
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (7,'Zapateria La Sorpreso',14,NULL,NULL);
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (8,'Pollerias \"El Chino\"',12,NULL,NULL);
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (9,'Ricos Pollos',12,NULL,NULL);
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (10,'MONDEL CARNES FRIAS',1,NULL,NULL);
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (11,'Vinos Y Licores D\'moy',2,NULL,NULL);
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (12,'Dulceria Memo',13,NULL,NULL);
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (13,'Floreria La Orquidea',7,NULL,NULL);
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (14,'Panaderia Olmeca',1,NULL,NULL);
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (15,'Farmacia mina',3,NULL,NULL);
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (16,'Abarrotes Racasti',1,NULL,NULL);
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (17,'Distribuidora de motores marinos Gafi sa de cv (YAMAHA)',10,NULL,NULL);
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (18,'Auto Refacciones Veracruz',10,NULL,NULL);
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (19,'Abarrotes \"la subidita\"',1,NULL,NULL);
INSERT INTO `establishments` (`id`,`name`,`category_establishment_id`,`created_at`,`updated_at`) VALUES (20,'Cremeria La Zamoreña',1,NULL,NULL);
