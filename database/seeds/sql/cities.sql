/*
-- Query: SELECT * FROM jaiimesend.cities
LIMIT 0, 1000

-- Date: 2019-09-29 16:57
*/
INSERT INTO `cities` (`id`,`name`,`created_at`,`updated_at`) VALUES (1,'Poza Rica',NULL,NULL);
INSERT INTO `cities` (`id`,`name`,`created_at`,`updated_at`) VALUES (2,'Gutierrez Zamora',NULL,NULL);
INSERT INTO `cities` (`id`,`name`,`created_at`,`updated_at`) VALUES (3,'Papantla',NULL,NULL);
INSERT INTO `cities` (`id`,`name`,`created_at`,`updated_at`) VALUES (4,'San Rafael',NULL,NULL);
INSERT INTO `cities` (`id`,`name`,`created_at`,`updated_at`) VALUES (5,'Tecolutla',NULL,NULL);
INSERT INTO `cities` (`id`,`name`,`created_at`,`updated_at`) VALUES (6,'Alamo',NULL,NULL);
