/*
-- Query: SELECT * FROM jaiimesend.category_establishments
LIMIT 0, 1000

-- Date: 2019-09-29 16:40
*/
INSERT INTO `category_establishments` (`id`,`name`,`created_at`,`updated_at`) VALUES (1,'Miscelaneas',NULL,NULL);
INSERT INTO `category_establishments` (`id`,`name`,`created_at`,`updated_at`) VALUES (2,'Licores',NULL,NULL);
INSERT INTO `category_establishments` (`id`,`name`,`created_at`,`updated_at`) VALUES (3,'Farmacia',NULL,NULL);
INSERT INTO `category_establishments` (`id`,`name`,`created_at`,`updated_at`) VALUES (4,'Tecnologia',NULL,NULL);
INSERT INTO `category_establishments` (`id`,`name`,`created_at`,`updated_at`) VALUES (5,'Veterinarias',NULL,NULL);
INSERT INTO `category_establishments` (`id`,`name`,`created_at`,`updated_at`) VALUES (6,'Bebes y Niños',NULL,NULL);
INSERT INTO `category_establishments` (`id`,`name`,`created_at`,`updated_at`) VALUES (7,'Floreria',NULL,NULL);
INSERT INTO `category_establishments` (`id`,`name`,`created_at`,`updated_at`) VALUES (8,'Jugueterias',NULL,NULL);
INSERT INTO `category_establishments` (`id`,`name`,`created_at`,`updated_at`) VALUES (9,'Papeleria',NULL,NULL);
INSERT INTO `category_establishments` (`id`,`name`,`created_at`,`updated_at`) VALUES (10,'Autos y Ferreterias',NULL,NULL);
INSERT INTO `category_establishments` (`id`,`name`,`created_at`,`updated_at`) VALUES (11,'Contruccion',NULL,NULL);
INSERT INTO `category_establishments` (`id`,`name`,`created_at`,`updated_at`) VALUES (12,'Pollerias',NULL,NULL);
INSERT INTO `category_establishments` (`id`,`name`,`created_at`,`updated_at`) VALUES (13,'Fiestas y Dulces',NULL,NULL);
INSERT INTO `category_establishments` (`id`,`name`,`created_at`,`updated_at`) VALUES (14,'Zapaterias',NULL,NULL);
