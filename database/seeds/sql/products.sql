/*
-- Query: SELECT * FROM jaiimesend.products
LIMIT 0, 1000

-- Date: 2019-09-29 17:46
*/
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (13,'Salchichas','FUD','250 gr','35','Salchicha de Pavo',12,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (14,'Jamon','FUD','250 gr','30','Jamon de pavo',12,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (15,'Manzana Royal','Royal','1 kg','34.90','Manzana Roja',2,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (16,'Limon','NA','1 kg','20','Limon verde',2,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (17,'Platano','NA','1 kg','15','Platano amarillo',2,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (18,'Tomate','NA','1kg','15','Tomate Rojo',2,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (19,'Cebolla','NA','1kg','25','Cebolla blanca',2,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (20,'Chile jalapeño','NA','1kg','20','Chile Jalapeño',2,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (21,'Media crema deslactosada','Nestle','190 gr','12.90','Media crema deslactosada Nestle',1,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (22,'Queso crema philadelphia','Philadelfia','150gr','12.50','Queso crema philadelphia',1,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (23,'Media cema','Alpura','150gr','11.90','Media crema Alpura 150 gr',1,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (24,'Leche evaporada','Carnation','360ml','13.30','Leche evaporada carnation 360ml',1,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (25,'Margarina sin sal light','Primavera','400gr','53','Margarina sin sal light primavera 400gr',1,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (26,'Agua Epura','Epura','1.5 L','13','Agua Epura 1.5 L',3,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (27,'Agua Natural Epura 600 mL x 6','Epura','600ml x 6','27.90','Agua Natural Epura 600 mL x 6',3,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (28,'Agua Ciel Natural 1.5 L','Ciel','1.5 L','12.70','Agua Ciel Natural 1.5 L',3,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (29,'Agua Epura Natural 1 L','Epura','1 L','7.70','Agua Epura Natural 1 L',3,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (30,'Té Helado Lipton Verde Cítricos Botella 600 mL','Lipton','600 ml','13','Té Helado Lipton Verde Cítricos Botella 600 mL',3,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (31,'Bebida Hidratante Gatorade Lima Limón 600 mL','Gatorade','600 ml','18','Bebida Hidratante Gatorade Lima Limón 600 mL',6,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (32,'Bebida Isotónica Gatorade Active Water Fresa Kiwi ','Gatorade','500 ml','12','Bebida Isotónica Gatorade Active Water Fresa Kiwi 500 mL',6,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (33,'Bebida Hidratante Gatorade Ponche de Frutas','Gatorade','600 ml','18','Bebida Hidratante Gatorade Ponche de Frutas 600 mL',6,NULL,NULL);
INSERT INTO `products` (`id`,`name`,`brand`,`size`,`price`,`description`,`category_products_id`,`created_at`,`updated_at`) VALUES (34,'Jugo Jumex Piña Tetrapack','Jumex','1.89 L','29.90','Jugo Jumex Piña Tetrapack 1.89 L',4,NULL,NULL);
