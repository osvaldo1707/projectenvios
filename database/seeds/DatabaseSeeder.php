<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProductsSeeder::class);
        $this->call(EstablishmentsSeeder::class);
         $this->call(CategoryEstablishmentsSeeder::class);
         $this->call(CategoryProductsSeeder::class);
         $this->call(CitiesSeeder::class);
         $this->call(TypeRecidencesSeeder::class);

    }
}
