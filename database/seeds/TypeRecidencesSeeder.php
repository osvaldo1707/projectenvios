<?php

use Illuminate\Database\Seeder;
use App\TypeRecidence;
class TypeRecidencesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TypeRecidence::truncate();
        $path = base_path().'/database/seeds/sql/type_recidences.sql';
        $sql = file_get_contents($path);
        DB::unprepared($sql);
    }
}
