<?php

use Illuminate\Database\Seeder;
use App\Establishment;

class EstablishmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Establishment::truncate();
        $path = base_path().'/database/seeds/sql/establishments.sql';
        $sql = file_get_contents($path);
        DB::unprepared($sql);
    }
}
