<?php

use Illuminate\Database\Seeder;
use App\Products;
class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Products::truncate();
        $path = base_path().'/database/seeds/sql/products.sql';
        $sql = file_get_contents($path);
        DB::unprepared($sql);
    }
}
