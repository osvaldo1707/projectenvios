@extends('pages.structure.layout')
@section('content')
<div class="login">
    <div class="container">
        <h2>Inciar Sesión</h2>
    
        <div class="login-form-grids animated wow slideInUp" data-wow-delay=".5s">
            {{ Form::open(array('route' => 'login')) }}
                <input name="_token" type="hidden" value="{{ csrf_token() }}" />

                {{Form::email('email', null,[
                                            'placeholder' => 'Correo Electronico',
                                            'required' => 'required',])}}
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                {{Form::password('password', null,[
                                            'placeholder' => 'Contraseña',
                                            'required' => 'required'])}}
                
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <input type="submit" value="Login">
            {{ Form::close() }}
        </div>
        <h4>Nuevos Usuarios</h4>
        <p><a href="registered.html">Registrate Aqui!!</a> (O) Regresar a pagina anterior <a href="index.html">Home<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a></p>
    </div>
</div>
@endsection