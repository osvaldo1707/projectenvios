@extends('pages.structure.layout')
@section('content')

<div class="register">
    <div class="container">
        <h2>{{$title_form_user}}</h2>

        {{Form::model($user, array('route' => array('user.update', encrypt($user->id))))}}

            {{ method_field('PUT') }}

            @include('pages/category/user/form')

        {{Form::close()}}
        
    </div>
</div>


@endsection