@extends('pages.structure.layout')
@section('content')

<div class="register">
    <div class="container">
        <h2>{{$title_form_user}}</h2>
        {{ Form::open(array('route' => 'user.store')) }}
            @include('pages/category/user/form')
        {{ Form::close() }}
    </div>
</div>


@endsection
