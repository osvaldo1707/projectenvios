<div class="login-form-grids">
    <h5>Informacion Personal</h5>
    <input name="_token" type="hidden" value="{{ csrf_token() }}" />

        {{Form::text('name', null,[
                                    'placeholder' => 'Nombre del Usuario',
                                    'required' => 'required',])}}
        <div class="invalid-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
            {{ $errors->first('name') }}
        </div>
        {{Form::text('first_lastname', null,[
                                            'placeholder' => 'Apellido Paterno',
                                            'required' => 'required',])}}
        <div class="invalid-feedback {{ $errors->has('first_lastname') ? 'has-error' : '' }}">
            {{ $errors->first('first_lastname') }}
        </div>
        {{Form::text('second_lastname', null,[
                                            'placeholder' => 'Apellido Materno', 
                                            'required' => 'required',])}}
        <div class="invalid-feedback {{ $errors->has('second_lastname') ? 'has-error' : '' }}">
            {{ $errors->first('second_lastname') }}
        </div>
        {{Form::text('phone', null,[
                                    'placeholder' => 'Telefono', 'required' => 'required',])}}
        <div class="invalid-feedback {{ $errors->has('phone') ? 'has-error' : '' }}">
            {{ $errors->first('phone') }}
        </div>
        <h6>Información de Inicio de Sesión </h6>
            {{Form::email('email', null,[
                'placeholder' => 'Correo Electronico', 
                                         'required' => 'required',])}}
            <div class="invalid-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                {{ $errors->first('email') }}
            </div>           
            {{Form::password('password', null,[
                'placeholder' => 'Contraseña', 'required' => 'required',])}}
            <div class="invalid-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                {{ $errors->first('password') }}
            </div>
            <input type="submit" value="{{$btn_submit}}">

</div>