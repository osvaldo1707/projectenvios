<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{

    protected $required = 'required';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id_user = $this->route('user');

        if($id_user)
            $id_user = (int) Crypt::decrypt($id_user);
        return [
            'name'   =>[
                $this->required
            ],
            'first_lastname'   =>[
                $this->required
            ],
            'second_lastname'   =>[
                $this->required
            ],
            'phone'   =>[
                $this->required
            ],
            'email'   =>[
                $this->required,
                Rule::unique('users')->ignore($id_user, 'id'),
            ],
        ];
    }
}
