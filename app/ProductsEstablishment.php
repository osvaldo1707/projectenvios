<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsEstablishment extends Model
{
    protected $fillable = [
        'name','products_id','establishment_id'
    ];
}
