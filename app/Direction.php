<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direction extends Model
{
    protected $fillable = [
        'name_street','type','city_id','residence_id','user_id'    
    ];
}
