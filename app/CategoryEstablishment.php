<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryEstablishment extends Model
{
    protected $fillable = [
        'name'    
    ];
}
