<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryProducts extends Model
{
    protected $fillable = [
        'name'    
    ];
}
