<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeRecidence extends Model
{
    protected $fillable = [
        'name'    
    ];
}
